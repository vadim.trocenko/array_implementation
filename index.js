/*

Choose your way:

class MyArray {} or function MyArray() {}

*/

function MyArray() {
  for (let i = 0; i < arguments.length; i++) {
    this[i] = arguments[i];
  };
  this.length = arguments.length;
};

MyArray.prototype.push = function () {
  for (let i = 0; i < arguments.length; i++) {
    this[this.length] = arguments[i];
    this.length = this.length + 1;
  };
  return this.length;
};

MyArray.prototype.pop = function () {
  if (this.length === 0) {
    return;
  };

  const deletedItem = this[this.length - 1];
  this.length = this.length - 1;
  delete this[deletedItem];
  return deletedItem;
};

MyArray.prototype.forEach = function (arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    const item = arr[i];
    callback(item, i, arr);
  };
};

MyArray.prototype.map = function (callback) {
  const newMapArr = new MyArray();
  for (let i = 0; i < this.length; i++) {
    const item = this[i];
    const callbackResult = callback(item, i, this);
    newMapArr[i] = callbackResult;
  };
  return newMapArr;
};

MyArray.prototype.filter = function (callback) {
  const filteredArr = new MyArray();
  for (let i = 0; i < this.length; i++) {
    const item = this[i];
    const callbackResult = callback(item, i, this);
    if (callbackResult) {
      filteredArr.push(item);
    };

  };
  return filteredArr;
};

MyArray.prototype.reduce = function (callback, initialValue) {
  acc = initialValue
  for (let i = 0; i < this.length; i++) {
    const item = this[i];
    const callbackResult = callback(acc, item, i, this);
    acc = callbackResult;
  };
  return acc;
};

MyArray.prototype.toString = function (arr) {
  const string = '';
  for (let i = 0; i < arr.length; i++) {
    const item = arr[i];
    string = item + ',' + string;
  };
  return string;
};

MyArray.from = function (value) {
  const myArray = new MyArray();
  for (let i = 0; i < value.length; i++) {
    const item = value[i];
    myArray.push(item);
  };
  return myArray;
};

MyArray.prototype.sort = function(compareFn) {
  if (!compareFn) {
    compareFn = (a,b) => b-a;
  };

  if (compareFn) {
    for (let i = this.length -1; i > 0; i--) {
      for(let j = 0; j < i; j++) {
        if (compareFn(this[j], this[j+1]) < 0) {
          const temp = this[j];
          this[j] = this[j + 1];
          this[j + 1] = temp;
        };
      };
    };
  };

  return this;
};

const arr = new MyArray(1, 4);